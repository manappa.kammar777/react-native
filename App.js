/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import Cart from './src/Cart';
import Home from './src/Home';
import Plp from './src/Plp';
import Pdp from './src/Pdp';
import Login from './src/Login';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      // <Cart />
      <Router hideNavBar= "true">
        <Scene key="root">
          <Scene key="Cart" component={Cart} title="Cart" />
          <Scene key="Home" component={Home} title="Home"  initial={true}/>
          <Scene key="Plp" component={Plp} title="Plp"/>
          <Scene key="Pdp" component={Pdp} title="Pdp"    />
          <Scene key="Login" component={Login} title="Login" />
        </Scene>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
