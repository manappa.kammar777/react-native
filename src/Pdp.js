import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Image,ScrollView,Dimensions, Modal, TouchableOpacity,Button,TouchableHighlight } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import Youmayalsolike from './Youmayalsolike';
import ImageZoom from 'react-native-image-pan-zoom';

class Pdp extends Component{
  constructor(){
    super()
    this.state={
      count:0,
      taptozoom:false,
      modalVisible: false,
      zoomimage:undefined
    }
    this.counteveluate = this.counteveluate.bind(this);
    this.taptozoomfun = this.taptozoomfun.bind(this);
  }
  counteveluate(data){
      this.setState({
        count: (data == 'plus') ? this.state.count+1 : this.state.count-1
      })
  }
  taptozoomfun(src){
    this.setState({
      zoomimage:src,
      taptozoom: (this.state.taptozoom === true) ?false:true,
      modalVisible:(this.state.modalVisible===true) ? false: true
    })
  }
render(){
    const images = [
        'https://placeimg.com/640/640/nature',
        'https://placeimg.com/640/640/people',
        'https://placeimg.com/640/640/animals',
        'https://placeimg.com/640/640/beer',
      ];
return(
<ScrollView>
<View style={{backgroundColor:'#fff'}}>

  <View style={styles.headerpdpimage}>
    <ImageSlider
          loopBothSides
          autoPlayWithInterval={3000}
          images={images}
          customSlide={({ index, item, style, width }) => (
            <View key={index} style={[style, styles.customSlide]}>
              <Image source={{ uri: item }} style={{height:200,width:'100%'}} />
              
              {this.state.taptozoom && 
                <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                >
                <ImageZoom 
                        cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={Dimensions.get('window').width}
                        imageHeight={200}
                        >
                <Image source={{uri:this.state.zoomimage}} style={{height:200,width:'100%'}} />
                </ImageZoom>
                  <View style={{position:'absolute',right:10,top:10}}>
                    <TouchableHighlight onPress={this.taptozoomfun.bind(this,item)}>
                      <Image source={require('./assets/cancel.png')} style={{height:25,width:25}}/>
                    </TouchableHighlight>
                  </View>
                </Modal>
              }

              
                <View style={{position:'absolute',right:10,top:0}}>
                <TouchableHighlight onPress={this.taptozoomfun.bind(this,item)}>
                  <Text style={{color:'#767676'}}>Tap zoom <Image source={require('./assets/maximize.png')} style={{height:25,width:25}}/></Text>
                  </TouchableHighlight>
                </View>
             



            </View>
          )}
          customButtons={(position, move) => (
            <View style={styles.buttons}>
              {images.map((image, index) => {
                return (
                  <TouchableHighlight
                    key={index}
                    underlayColor="#b3b3b3"
                    onPress={() => move(index)}
                    style={styles.button}
                  >
                    <Text style={position === index && styles.buttonSelected}>
                      {/* {index + 1} */}.
                    </Text>
                  </TouchableHighlight>
                );
              })}
            </View>
          )}
        />
    <Image style={styles.pdpnewimage} source={require('./assets/new.png')}/>
  </View>
  <View style={styles.pdpheadertext}>
      <Text style={styles.pdpheadertextinside}>Long Sleeve Draw Cord Closure Printed Hoodie White W!8SWT34</Text>
  </View>
  <View style={styles.pdpaed}>
      <View style={styles.grayaed}>
        <Text style={styles.grayaedtext}>AED 75</Text>
      </View>
      <View style={styles.blueaed}>
        <Text style={styles.blueaedtext}>AED 65</Text>
        <Text style={styles.blueaednumber}>(10%)</Text>
      </View>
      <View style={styles.likeaed}>
        <Image source={require('./assets/like.png')} style={{height:20,width:20}} />
      </View>
  </View>
  
  <View style={styles.pdpcolour}>
    <View style={styles.horizontalLine}/>
    <Text style={styles.pdpcolourtext}>Colour</Text>
    <View style={styles.gridviewofimagesmain}>
      <View style={styles.gridofimages}>
        <Image source={require('./assets/kids.jpg')} style={styles.gridimg} />
      </View>
      <View style={styles.gridofimages}>
        <Image source={require('./assets/kids.jpg')} style={styles.gridimg} />
      </View>
    </View>
  </View>

  <View style={styles.sizeguidemain}>
    <Text style={styles.size}>Size <Text style={styles.sizetext}>(Size Guide)</Text></Text>
    <View style={{flexDirection:'row'}}>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>2</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>4</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>6</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>8</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>10</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>12</Text>
      </View>
      <View style={styles.pdpsizemain}>
        <Text style={styles.pdpsizetext}>14</Text>
      </View>
    </View>

    <View style={styles.pdpquantity}>
      <Text style={styles.pdpquantitytext}>Quantity</Text>
      <View style={styles.pdpquantitymain}>
          <View style={styles.pdpqunatitynumber}>
              <Text style={styles.pdpquantityinside}>{this.state.count}</Text>
          </View>
          <TouchableOpacity style={styles.pdpqunatitynumber} onPress={this.counteveluate.bind(this,'plus')}>
          <View>
              <Text style={styles.pdpquantityinside}>
                <Image source={require('./assets/plus.png')} style={styles.pdpicons} />
              </Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.pdpqunatitynumber} onPress={this.counteveluate.bind(this,'minus')}>
          <View>
              <Text style={styles.pdpquantityinside}>
                <Image source={require('./assets/minus.png')} style={styles.pdpicons} />
              </Text>
          </View>
          </TouchableOpacity>
      </View>
    </View>

    <View style={styles.transport}>
      <View style={styles.delivered}>
        <View style={styles.deliveredleft}>
          <Image source={require('./assets/truck.png')} style={styles.deliveryicon} />
        </View>
        <View style={styles.deliveredright}>
          <Text style={styles.deliveredText}>Delivered 3-5 business days</Text>
        </View>
      </View>
      <View style={styles.delivered}>
        <View style={styles.deliveredleft}>
          <Image source={require('./assets/left.png')} style={styles.deliveryicon} />
        </View>
        <View style={styles.deliveredright}>
          <Text style={styles.deliveredText}>This product has 14 Day return Or exchange policy</Text>
        </View>
      </View>
      <View style={styles.delivered}>
        <View style={styles.deliveredleft}>
          <Image source={require('./assets/logistics.png')} style={styles.deliveryicon} />
        </View>
        <View style={styles.deliveredright}>
          <Text style={styles.deliveredText}>Free shipping on orders above AED 150</Text>
        </View>
      </View>
    </View>

    <View style={styles.pincode}>
      <View style={styles.pincodeleft}>
        <TextInput style={styles.pincodeinput} placeholder="Enter Pincode"/>
      </View>
      <View style={styles.pincodebutton}>
        <Button title="CHECK" color="#4d9ad7" style={styles.pincodeinsidebutton}/>
      </View>
    </View>

    <View style={styles.horizontalLine}/>

    <View style={styles.product}>
      <Text style={styles.productheader}>PRODUCT DESCRIPTION</Text>
      <Text style={styles.productheader}>
        Ensure yourself sweat jacket-most level of comfort by getting this olive colored sweat
        jacket for women by R&B. It is an essential pick to stay warm and cozy this season, as it 
        is made from soft fabric. This sweat jacket will also lend you endless style when you 
        team it with a pair of jeggings and sneakers.{'\n'}
        Height 177 cm, Bust 89 cm, Waist 62 cm, Hips 92 cm Model is wearing size S.
      </Text>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>SKU:</Text>
        <Text style={styles.attributeright}>W18JKT52Y</Text>
      </View>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>Gender:</Text>
        <Text style={styles.attributeright}>Women</Text>
      </View>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>Color:</Text>
        <Text style={styles.attributeright}>Green</Text>
      </View>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>Sleeve Length:</Text>
        <Text style={styles.attributeright}>Long Sleeve</Text>
      </View>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>Neck Line:</Text>
        <Text style={styles.attributeright}>High Neck</Text>
      </View>
      <View style={styles.attribute}>
        <Text style={styles.attributeleft}>Brand Name:</Text>
        <Text style={styles.attributeright}>R&B</Text>
      </View>
    </View>

    <View style={styles.horizontalLine}/>

    <Youmayalsolike />

    <View style={styles.horizontalLine}/>

    <View style={styles.customer}>
      <Text style={styles.customerstext}>CUSTOMERS ALSO VIEWED</Text>
      <View style={styles.customermain}>
        
        <View style={styles.recmain}>
          <Image style={styles.recimage} source={require('./assets/1.jpg')}/>
          <View>
              <Text style={styles.recimageheader}>Long Sleeves Jacket Closure and Hood</Text>
              <View style={styles.recflow}>
                  <Text style={styles.recaed}>AED 36</Text>
                  <Text style={styles.recaedgray}>AED 36</Text>
              </View>
          </View>
        </View>
        <View style={styles.recmain}>
          <Image style={styles.recimage} source={require('./assets/1.jpg')}/>
          <View>
              <Text style={styles.recimageheader}>Long Sleeves Jacket Closure and Hood</Text>
              <View style={styles.recflow}>
                  <Text style={styles.recaed}>AED 36</Text>
                  <Text style={styles.recaedgray}>AED 36</Text>
              </View>
          </View>
        </View>

      </View>
    </View>

  </View>
 
</View>

<View>
  <TouchableOpacity style={styles.addtocart} ><Text style={styles.addtocarttext}>Add to cart</Text></TouchableOpacity>
</View>
</ScrollView>   
)
}
}

export default Pdp;

const styles = StyleSheet.create({
    pdpimage:{
        width:'100%',
        height:300,
        resizeMode: 'contain',
    },
    pdpnewimage:{
        position:'absolute',
        left:10,
        bottom:0
    },
    customSlide: {
        backgroundColor: 'green',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttons: {
        zIndex: 1,
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    button: {
        margin: 3,
        opacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'#b3b3b3',
        borderRadius:50,
        width:10,
        height:10
    },
    buttonSelected: {
      opacity: 1,
      color: '#4c99d6',
      backgroundColor:'#4c99d6',
      borderRadius:50,
      width:10,
      height:10
    },
    headerpdpimage:{
      position:'relative',
      paddingTop:10,
      paddingLeft:10,
      paddingRight:10,
    },
    pdpheadertext:{
      padding:10
    },
    pdpheadertextinside:{
      color:'#000',
      fontSize:16,
      fontWeight:'500'
    },
    pdpaed:{
      flexDirection:'row',
      padding:10
    },
    grayaed:{
      paddingRight:10,
    },
    grayaedtext:{
      fontSize:16,
      textDecorationLine: 'line-through',
      paddingTop:5
    },
    blueaed:{
      paddingLeft:10,
      paddingRight:10,
      flexDirection:'row',
      flexGrow: 1
    },
    blueaedtext:{
      fontSize:20,
      color:'#4c98d5',
      paddingRight:10
    },
    blueaednumber:{
      fontSize:16,
      color:'red',
      paddingTop:5
    },
    likeaed:{

    },
    horizontalLine: {
      borderBottomColor: '#dedede',
      borderBottomWidth: 1,
      marginTop: 5
    },
    gridviewofimagesmain:{
      flexDirection:'row'
    },
    gridofimages:{
      marginRight:10
    },
    pdpcolour:{
      paddingLeft:10,
      paddingRight:10,
      paddingBottom:10
    },
    pdpcolourtext:{
      paddingTop:10,
      paddingBottom:5,
      fontSize:16,
      color:'#31384a'
    },
    gridimg:{
      height:80,
      width:80
    },
    pdpsizemain:{
      backgroundColor:'#f1f1f1',
      width:50,
      height:30,
      justifyContent: "center",
      alignItems: "center",
      marginRight:10
    },
    pdpsizetext:{
      color:'#a3a3a3',
      textAlign:'center',
      textAlignVertical: "center"
    },
    sizeguidemain:{
      padding:10
    },
    size:{
      fontSize:16,
      paddingBottom:10,
      color:'#31384a'
    },
    sizetext:{
      color:'#4e9dd7',
      paddingLeft:10,
      paddingBottom:10
    },
    pdpquantitytext:{
      fontSize:16,
      color:'#31384a',
      paddingBottom:10
    },
    pdpquantity:{
      paddingTop:10,
      paddingBottom:10
    },
    pdpquantitymain:{
      flexDirection:'row',
    },
    pdpqunatitynumber:{
      width:'33.33%',
      justifyContent:'center',
      alignItems:'center',
      height:35,
      borderWidth:0.5,
      borderRightColor:'#bebebe',
      borderBottomColor:'#bebebe',
      borderTopColor:'#bebebe',
      borderLeftColor:'#bebebe'
    },
    pdpquantityinside:{
      textAlign:'center',
      textAlignVertical:'center',
      fontSize:16
    },
    pdpicons:{
      height:16,
      width:16
    },
    transport:{

    },
    delivered:{
      flexDirection:'row',
      paddingTop:10
    },
    deliveredleft:{
      paddingRight:10
    },
    deliveredright:{
      justifyContent:'center',
      alignSelf: "center"
    },
    deliveredText:{
      fontSize:16,
      textAlignVertical:'center'
    },
    deliveryicon:{
      height:35,
      width:35
    },
    pincode:{
      flexDirection:'row',
      paddingTop:10,
      paddingBottom:10
    },
    pincodeleft:{
      width:'60%',
      paddingRight:10,
      paddingLeft:10
    },
    pincodebutton:{
      width:'40%',
      justifyContent:'center',
      alignSelf: "center"
    },
    pincodeinput:{
      borderBottomColor:'#ccc',
      borderBottomWidth:0.5,
      fontSize:16
    },
    pincodeinsidebutton:{
      textAlignVertical:'bottom',
      fontSize:16
    },
    product:{
      paddingTop:20,
      paddingBottom:20
    },
    productheader:{
      fontSize:16,
      paddingBottom:10
    },
    attribute:{
      flexDirection:'row',
      paddingTop:5
    },
    attributeleft:{
      width:'40%',
      fontSize:16,
    },
    attributeright:{
      width:'60%',
      fontSize:16,
    },
    customerstext:{
      fontSize:20,
      color:'#000',
      fontWeight:'500',
      paddingTop:20,
      paddingBottom:5
    },
    customermain:{
      flexDirection:'row'
    },
    addtocart:{
      width: '100%',
      height: 60,
      alignItems: 'center',
      justifyContent: 'center',
      textAlignVertical:'center',
      backgroundColor:"#4d9ad7"
    },
    addtocarttext:{
      fontSize:24,
      color:'#fff',
      fontWeight:'500'
    },


  recimage:{
    height:142,
    width: 142
  },
  recmain:{
      width:'50%',
      paddingTop:10,
      paddingBottom:10,
      position:'relative'
  },
  recimageheader:{
      fontSize:16,
      color:'#000',
      paddingTop:4,
      paddingBottom:4,
      paddingRight:10
  },
  recflow:{
      flexDirection:'row',
      paddingRight:10
  },
  recaed:{
      width:'50%',
      fontSize:18,
      color:'#000',
      fontWeight:'500'
  },
  recaedgray:{
      textAlign:'right',
      width:'50%',
      color:'#767676',
      textDecorationLine:'line-through',
      textDecorationStyle:'solid',
      fontSize:17
  },
  recdiv:{
      flexDirection:'row'
  }
})