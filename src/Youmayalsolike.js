import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Image,ScrollView,LayoutAnimation, UIManager, TouchableOpacity,Button} from 'react-native';

class Youmayalsolike extends Component{
render(){
return(
    <View>
    <Text style={styles.recheader}>YOU MAY ALSO LIKE</Text>
    <View style={styles.recdiv}>
    <View style={styles.recmain}>
        <Image style={styles.recimage} source={require('./assets/1.jpg')}/>
        <Image style={styles.newimage} source={require('./assets/new.png')}/>
        <View>
            <Text style={styles.recimageheader}>Long Sleeves Jacket Closure and Hood</Text>
            <View style={styles.recflow}>
                <Text style={styles.recaed}>AED 36</Text>
                <Text style={styles.recaedgray}>AED 36</Text>
            </View>
        </View>
    </View>
    <View style={styles.recmain}>
        <Image style={styles.recimage} source={require('./assets/1.jpg')}/>
        <Image style={styles.newimage} source={require('./assets/new.png')}/>
        <View>
            <Text style={styles.recimageheader}>Long Sleeves Jacket Closure and Hood</Text>
            <View style={styles.recflow}>
                <Text style={styles.recaed}>AED 36</Text>
                <Text style={styles.recaedgray}>AED 36</Text>
            </View>
        </View>
    </View>
    </View>
</View>
)
}
}

const styles = StyleSheet.create({
recheader:{
    fontWeight:'bold',
    fontSize:20,
    color:'#000',
    marginTop:20,
    marginBottom:10
},
recimage:{
    height:142,
    width: 142
},
recmain:{
    width:'50%',
    paddingTop:10,
    paddingBottom:10,
    position:'relative'
},
recimageheader:{
    fontSize:16,
    color:'#000',
    paddingTop:4,
    paddingBottom:4,
    paddingRight:10
},
recflow:{
    flexDirection:'row',
    paddingRight:10
},
recaed:{
    width:'50%',
    fontSize:18,
    color:'#000',
    fontWeight:'500'
},
recaedgray:{
    textAlign:'right',
    width:'50%',
    color:'#767676',
    textDecorationLine:'line-through',
    textDecorationStyle:'solid',
    fontSize:17
},
recdiv:{
    flexDirection:'row'
},
newimage:{
    position:'absolute',
    top:130,
    left:10,
    zIndex:9
}
})

export default Youmayalsolike;