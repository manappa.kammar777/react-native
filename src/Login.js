import React, {Component} from 'react';
import {CheckBox, StyleSheet, Text, View,TextInput,Image,ScrollView,Dimensions, Modal, TouchableOpacity,Button, TouchableHighlight } from 'react-native';
import Searchproduct from './Searchproduct';

class Login extends Component{
render(){
return(
<ScrollView>
<View style={styles.loginmain}>
    <Searchproduct />
    <View style={styles.secondblock}>
    <Text style={styles.login}>LOGIN</Text>
    <View style={styles.loginblock}>
        <View style={styles.innerloginblock}>
            <Text style={styles.loginwelocome}>Welocome back</Text>
            <Text style={styles.simplytext}>Simply sign in to access your R&B account.</Text>
            <View style={{paddingLeft:15,paddingRight:15}}>
                <Text style={styles.emaillogin}>Email</Text>
                <TextInput style={styles.emailtextinput} onChangeText={(text) => this.setState({text})} value='' />
                <Text style={styles.emaillogin}>Password</Text>
                <TextInput style={styles.emailtextinput} onChangeText={(text) => this.setState({text})} value='' />
                <View style={{flexDirection:'row'}}>
                    <View style={styles.logindivide}>
                        <CheckBox style={styles.logincheckbox} title='Click Here' checked={false} />
                        <Text style={styles.remember}>Remember Me</Text>
                    </View>
                    <View style={styles.logindivide}>
                        <Text style={styles.forgot}>Forgot Your Password?</Text>
                    </View>
                </View>
                <View style={{alignSelf: 'flex-end'}}>
                    <TouchableHighlight style={styles.loginbutton}>
                        <Text style={styles.logintextbutton}>Login</Text>
                    </TouchableHighlight>
                </View>
                <View style={styles.loginaccount}>
                    <Text>Don't have an account? </Text>
                    <Text style={styles.signuptext}>Sign up for one</Text>
                </View>
            </View>
            <View style={styles.social}>
                <Text style={styles.networktext}>Login or Register with social media</Text>
                <View style={styles.belowlogin}>
                    <View style={styles.leftgoogle}>
                    <TouchableHighlight >
                        <View style={styles.googlemain}>
                            <Image style={styles.googleimage} source={require('./assets/google.png')}/>
                            <Text style={styles.googletext}>GOOGLE</Text>
                        </View>
                    </TouchableHighlight>
                    </View>
                    <View style={styles.rightgoogle}>
                    <TouchableHighlight>
                        <View style={styles.facebookmain}>
                            <Image style={styles.googleimage} source={require('./assets/facebook.png')}/>
                            <Text style={styles.googletext}>FACEBOOK</Text>
                        </View>
                    </TouchableHighlight>
                    </View>
                </View>
            </View>
        </View>
    </View>
    </View>
</View>
</ScrollView>
)
}
}

export default Login;

const styles = StyleSheet.create({
    loginmain:{
        backgroundColor:'#fff'
    },
    secondblock:{
        padding:10
    },
    login:{
        color:'#000',
        fontSize:20,
        fontWeight:'500'
    },
    loginblock:{
        marginTop:10,
        borderWidth:0.5,
        borderColor:'#666'
    },
    innerloginblock:{
        paddingBottom:15
    },
    loginwelocome:{
        color:'#000',
        fontSize:16,
        fontWeight:'500',
        paddingLeft:15,
        paddingRight:15,
        paddingTop:15
    },
    simplytext:{
        paddingLeft:15,
        paddingRight:15
    },
    emaillogin:{
        color:'#000',
        fontSize:14,
        fontWeight:'500',
        paddingTop:10,
        paddingBottom:5
    },
    emailtextinput:{
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    },
    logindivide:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingTop:10,
        paddingBottom:10
    },
    remember:{
        textAlignVertical:'center',
        fontWeight:'500',
        color:'gray'
    },
    forgot:{
        textAlignVertical:'center',
        paddingLeft:10,
        fontWeight:'500',
        color:'#4c98d5'
    },
    logincheckbox:{
        paddingLeft:0,
        marginLeft:-5
    },
    loginbutton:{
        backgroundColor:'#4e4e4e',
        width:100,
        padding:10
    },
    logintextbutton:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'500',
        fontSize:16
    },
    loginaccount:{
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row'
    },
    signuptext:{
        color:'#4e9dd7'
    },
    social:{
        borderTopWidth:0.5,
        borderColor:'gray'
    },
    networktext:{
        textAlign:'center',
        fontSize:14,
        fontWeight:'500',
        color:'#000',
        marginTop:10
    },
    belowlogin:{
        flexDirection:'row',
        marginTop:10,
        paddingLeft:20,
        paddingRight:20
    },
    googlemain:{
        backgroundColor:'#c94631',
        flexDirection:'row',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:5,
        paddingBottom:5,
        justifyContent:'center',
        alignItems:'center'
    },
    googleimage:{
        height:15,
        width:15,
        marginRight:5,
        // marginTop:3,
    },
    googletext:{
        color:'#fff',
        fontSize:12,
        paddingLeft:10,
        borderLeftWidth:0.5,
        borderColor:'gray'
    },
    facebookmain:{
        backgroundColor:'#49619d',
        flexDirection:'row',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:5,
        paddingBottom:5,
        justifyContent:'center',
        alignItems:'center'
    },
    leftgoogle:{
        width:'50%',
        paddingRight:10,
        paddingLeft:10
    },
    rightgoogle:{
        paddingLeft:10,
        paddingRight:10,
        width:'50%',
        alignSelf: 'flex-end'
    }
})