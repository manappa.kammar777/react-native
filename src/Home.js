import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Image,ScrollView,LayoutAnimation, UIManager, TouchableOpacity,Button} from 'react-native';
import Searchproduct from './Searchproduct';

class Home extends Component{
constructor(props) {
    super(props);
    this.state = { text: '' };
}
render(){
return(
    <ScrollView>
    <View style={{backgroundColor:'#fff'}}>
    <Searchproduct />

    <View>
    <Image style={styles.banner} source={require('./assets/home.jpg')}/>
    </View>

    <View style={styles.freeshipping}>
        <Image style={styles.vanImage} source={require('./assets/van.png')}/>
        <Text>Free shipping - on orders above AED 150</Text>
    </View>

    <View style={styles.category}>
        <View style={styles.categoryview}><Image style={styles.categoryImage} source={require('./assets/women.jpg')}/></View>
        <View style={styles.categoryview}><Image style={styles.categoryImage} source={require('./assets/men.jpg')}/></View>
        <View style={styles.categoryview}><Image style={styles.categoryImage} source={require('./assets/kids.jpg')}/></View>
    </View>

    <View style={styles.horizontalLine}/>

    <View>
        <Text style={styles.hotdeals}>HOT DEALS</Text>
        <View style={styles.hotmain}>
            <View style={styles.hotleft}>
                <Image style={styles.hotimage} source={require('./assets/women.jpg')}/>
            </View>
            <View style={styles.hotleft}>
                <Image style={styles.hotimage} source={require('./assets/men.jpg')}/>
            </View>
        </View>
    </View>

    </View>
    </ScrollView>
)
}
}
export default Home;

const styles = StyleSheet.create({
    shoppingText: {
      color: '#000',
      fontWeight: 'bold',
      fontSize: 20,
      marginTop:10
    },
    banner:{
        width:'100%',
        height:220
    },
    vanImage:{
        width:20,
        height:20,
        marginRight:10
    },
    freeshipping:{
        backgroundColor:'#fdfecc',
        padding:15,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize:14
    },
    category:{
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:5,
        paddingRight:5,
        flexDirection:'row'
    },
    categoryview:{
        width:'33.33%',
        paddingLeft:2,
        paddingRight:2,
    },
    categoryImage:{
        width:'100%',
        height:100,
        borderWidth:0.2,
        borderColor:'#0e0e0e'
    },
    horizontalLine: {
        borderBottomColor: '#dedede',
        borderBottomWidth: 1,
        marginTop: 5
    },
    hotdeals:{
        color:'#000',
        fontSize:18,
        fontWeight:'bold',
        padding:10,
        paddingTop:15
    },
    hotmain:{
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row'
    },
    hotleft:{
        width:'50%',
        paddingLeft:10,
        paddingRight:5
    },
    hotimage:{
        height:150,
        width:'100%'
    }
})