import React, {Component} from 'react';
import { StyleSheet,  View,TextInput,Image,ScrollView} from 'react-native';

class Searchproduct extends Component{
constructor(props) {
    super(props);
    this.state = { text: '' };
}    
render(){
return(
    <View style={styles.mainView}>
        <TextInput
        style={styles.textInput}
        onChangeText={(text) => this.setState({text})}
        value={this.state.text}
        placeholder="What are you looking for?"
        />
        <Image style={styles.searchImage} source={require('./assets/search.png')}/>
    </View>
)
}
}

export default Searchproduct;

const styles = StyleSheet.create({
    mainView: {
      padding: 10,
      position:'relative'
    },
    textInput: {
        height:40,
        borderColor: '#dedede',
        borderWidth:1,
        backgroundColor:'#f7f7f7',
        padding:10
    },
    searchImage: {
        width:20,
        height:20,
        position: 'absolute',
        right:20,
        top:20
    }
})