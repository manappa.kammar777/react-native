import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput,Image,ScrollView,LayoutAnimation, UIManager, TouchableOpacity,Button} from 'react-native';
import Searchproduct from './Searchproduct';
import Youmayalsolike from './Youmayalsolike';

class Cart extends Component{
constructor(props) {
    super(props);
    this.state = { text: '' ,expanded: false,coupon:''};
 
    if (Platform.OS === 'android') {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}
	
changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ expanded: !this.state.expanded });
}

render(){
return(
<ScrollView>
<View style={{backgroundColor:'#fff'}}>
<Searchproduct />
<Text style={styles.shoppingText}>SHOPPING CART</Text>
{/* <View style={styles.horizontalLine}/> */}

<View style={styles.grid}>
    <View style={styles.gridLeft}>
        <Image style={styles.leftImage} source={require('./assets/1.jpg')}/>
    </View>
    <View style={styles.gridRight}>
        <Text style={styles.headerText}>Long Sleeve Draw Cord Closure Printed Hoodie</Text>
        <Text style={styles.distance}>Color: <Text style={styles.color}>Green</Text></Text>
        <Text style={styles.distance}>Size: <Text style={styles.color}>14</Text></Text>
        <Text style={styles.aed}>AED 65</Text>
        <Text>Qty:</Text>
        <View style={{flexDirection:'row'}}>
            <View style={styles.qtyMain}><Text style={styles.qty}>1</Text></View>
            <View style={styles.rightimages}>
                <View><Image style={styles.like} source={require('./assets/like.png')}/></View>
                <View><Image style={styles.trash} source={require('./assets/trash.png')}/></View>
            </View>
        </View>
    </View>
</View>
<View style={styles.grid}>
    <View style={styles.gridLeft}>
        <Image style={styles.leftImage} source={require('./assets/1.jpg')}/>
    </View>
    <View style={styles.gridRight}>
        <Text style={styles.headerText}>Long Sleeve Draw Cord Closure Printed Hoodie</Text>
        <Text style={styles.distance}>Color: <Text style={styles.color}>Green</Text></Text>
        <Text style={styles.distance}>Size: <Text style={styles.color}>14</Text></Text>
        <Text style={styles.aed}>AED 65</Text>
        <Text>Qty:</Text>
        <View style={{flexDirection:'row'}}>
            <View style={styles.qtyMain}><Text style={styles.qty}>1</Text></View>
            <View style={styles.rightimages}>
                <View><Image style={styles.like} source={require('./assets/like.png')}/></View>
                <View><Image style={styles.trash} source={require('./assets/trash.png')}/></View>
            </View>
        </View>
    </View>
</View>

<View style={styles.horizontalLine}/>

<View>
    <TouchableOpacity activeOpacity={0.8} onPress={this.changeLayout} style={styles.Btn}>
        <Text style={styles.btnText}>Coupon</Text>
        <Text>Enter your coupon code here.</Text>
    </TouchableOpacity>
    <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden' }}>
    <View style={styles.couponMain}>
        <View style={styles.leftcoupon}>
            <TextInput
            style={styles.couponInput}
            onChangeText={(coupon) => this.setState({coupon})}
            value={this.state.coupon}
            placeholder="Coupon code"
            />
        </View>
        <View style={styles.rightcoupon}>
            <Button
            // onPress={onPressLearnMore} 
            style={styles.apply}
            title="Apply" />
        </View>
    </View>
    </View>
</View>

<View style={styles.horizontalLine}/>

<View style={styles.totalmain}>
    <View style={styles.totalrow}>
        <Text style={styles.totaltext}>Subtotal</Text>
        <Text style={styles.totaltextrt}>AED 65</Text>
    </View>
    <View style={styles.totalrow}>
        <Text style={styles.totaltext}>Shipping Charge</Text>
        <Text style={styles.totaltextrt}>AED 10</Text>
    </View>
    <View style={styles.totalrow}>
        <Text style={styles.totallt}>Total (Incl of VAT):</Text>
        <Text style={styles.totalrt}>AED 75</Text>
    </View>
    <Button title="Proceed To Check Out" />
</View>

<View style={styles.horizontalLine}/>

<Youmayalsolike />

</View>
</ScrollView>
)    
}
}

export default Cart;


const styles = StyleSheet.create({
    shoppingText: {
      color: '#000',
      fontWeight: 'bold',
      fontSize: 20,
      paddingLeft:10
    },
    horizontalLine: {
        borderBottomColor: '#dedede',
        borderBottomWidth: 1,
        marginTop: 5
    },
    leftImage: {
        width:120,
        height:180
    },
    grid: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        paddingTop: 15,
        paddingBottom: 15,
        borderTopColor: '#dedede',
        borderTopWidth: 1,
        marginTop: 5
    },
    gridLeft: {
        width:'40%',
        aspectRatio:1,
        paddingLeft:10
    },
    gridRight: {
        width:'60%',
        aspectRatio:1,
        paddingLeft:15,
        paddingRight:10
    },
    headerText:{
        color:'#000',
        fontSize:16
    },
    color:{
        color:'#000'
    },
    aed:{
        color:'#000',
        fontSize:16,
        paddingTop:10,
        paddingBottom:10
    },
    distance:{
        paddingTop:10
    },
    qty:{
        color:'#000',
        borderWidth:1,
        width:35,
        height:35,
        marginTop:5,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        padding:5
    },
    rightimages:{
        width:'50%',
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    like:{
        width:25,
        height:25,
        marginRight:10
    },
    trash:{
        width:25,
        height:25,
    },
    qtyMain:{
        width:'50%'
    },
    Btn: {
        padding: 10,
    },
    btnText: {
        color: '#000',
        fontSize: 16,
        fontWeight:'500'
    },
    couponMain:{
        padding:10,
        flexDirection:'row'
    },
    couponInput:{
        height:40,
        borderColor: '#dedede',
        borderWidth:1,
        backgroundColor:'#f7f7f7',
        padding:10
    },
    apply:{
        color:'#50a2d5',
        borderWidth:1,
        borderColor:'#50a2d5',
        backgroundColor:'#fff',
        height:40
    },
    leftcoupon:{
        width:'70%'
    },
    rightcoupon:{
        width:'30%'
    },
    totalmain:{
        padding:10,
        marginTop:10,
        marginBottom:10
    },
    totaltext:{
        fontSize:16,
        fontWeight:'bold',
        color:'#000',
        flexDirection:'row',
        width:'50%'
    },
    totaltextrt:{
        fontSize:16,
        fontWeight:'bold',
        color:'#000',
        flexDirection:'row',
        width:'50%',
        textAlign:'right'
    },
    totalrow:{
        flexDirection:'row'
    },
    totallt:{
        fontSize:18,
        fontWeight:'bold',
        color:'#000',
        flexDirection:'row',
        width:'60%',
        marginTop:10,
        marginBottom:10
    },
    totalrt:{
        fontSize:18,
        fontWeight:'bold',
        color:'#000',
        flexDirection:'row',
        width:'40%',
        textAlign:'right',
        marginTop:10,
        marginBottom:10
    },
    
  });