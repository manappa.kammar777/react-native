import React, {Component} from 'react';
import { StyleSheet, Text, View,CheckBox,Image,ScrollView, TouchableOpacity, TouchableHighlight, Picker} from 'react-native';
import Searchproduct from './Searchproduct';

class Plp extends Component{
constructor(props) {
    super(props);
    this.state = { text: '',sortby:false, filter:false, plplist:true };
    this.sortbyfun = this.sortbyfun.bind(this);
    this.filterfun = this.filterfun.bind(this);
    this.closelist = this.closelist.bind(this);
}
closelist(){
    this.setState({
        plplist:true,
        filter:false,
        sortby:false
    })
}
filterfun(){
    this.setState({
        plplist:false,
        filter:true,
        sortby:false
    })
}
sortbyfun(){
    this.setState({
        plplist:false,
        filter:false,
        sortby:true
    })
}
render(){
return(
<ScrollView>
<View style={{backgroundColor:'#fff'}}>

<Searchproduct />
{this.state.plplist === true && 
<View>
<View style={styles.flowDirection}>
    <Text style={styles.header}>Women Tops</Text>
    <Text style={styles.headerrt}>40 Products</Text>
</View>

<View style={styles.plpmain}>
    <View style={styles.plpgrid}>
        <View>
            <Image style={styles.plpimage} source={require('./assets/women.jpg')}/>
            <Image style={styles.newplpimage} source={require('./assets/new.png')}/>
            <Image style={styles.likeplpimage} source={require('./assets/like.png')}/>
        </View>
        <View>
            <Text style={styles.plpgridtext}>Long Sleeve Jacket Closure and Hood</Text>
            <Text style={styles.plpaed}>AED 36</Text>
        </View>
    </View>
    <View style={styles.plpgrid}>
        <View>
            <Image style={styles.plpimage} source={require('./assets/kids.jpg')}/>
            <Image style={styles.newplpimage} source={require('./assets/new.png')}/>
            <Image style={styles.likeplpimage} source={require('./assets/like.png')}/>
        </View>
        <View>
            <Text style={styles.plpgridtext}>Long Sleeve Jacket Closure and Hood</Text>
            <Text style={styles.plpaed}>AED 36</Text>
        </View>
    </View>
    <View style={styles.plpgrid}>
        <View>
            <Image style={styles.plpimage} source={require('./assets/1.jpg')}/>
            <Image style={styles.newplpimage} source={require('./assets/new.png')}/>
            <Image style={styles.likeplpimage} source={require('./assets/like.png')}/>
        </View>
        <View>
            <Text style={styles.plpgridtext}>Long Sleeve Jacket Closure and Hood</Text>
            <Text style={styles.plpaed}>AED 36</Text>
        </View>
    </View>
    <View style={styles.plpgrid}>
        <View>
            <Image style={styles.plpimage} source={require('./assets/women.jpg')}/>
            <Image style={styles.newplpimage} source={require('./assets/new.png')}/>
            <Image style={styles.likeplpimage} source={require('./assets/like.png')}/>
        </View>
        <View>
            <Text style={styles.plpgridtext}>Long Sleeve Jacket Closure and Hood</Text>
            <Text style={styles.plpaed}>AED 36</Text>
        </View>
    </View>
</View>

<View style={styles.changeproduct}>
    <TouchableOpacity style={styles.leftfilter} onPress={this.sortbyfun}> 
        <View style={{flexDirection:'row'}}>
            <Image source={require('./assets/sort.png')}/>
            <Text style={styles.filterText}>SORT BY</Text>
        </View>
    </TouchableOpacity>
    <TouchableOpacity style={styles.leftfilter} onPress={this.filterfun}>
        <View style={{flexDirection:'row'}}>
            <Image source={require('./assets/sort.png')}/>
            <Text style={styles.filterText}>FILTER</Text>
        </View>
    </TouchableOpacity>
</View>
</View>
}
{(this.state.filter === true)&&
<View>

<View style={styles.filtersmain}>
    <View style={styles.filterheaderleft}>
        <TouchableOpacity onPress={this.closelist}><Text style={styles.filterheadertextx}>X</Text></TouchableOpacity>
        <Text style={styles.filterheadertext}>FILTERS</Text>
    </View>
    <View style={styles.filterheaderright}>
        <TouchableHighlight style={styles.applybutton}>
            <Text style={{color:'#fff',textAlign:'center',fontWeight:'500'}}>APPLY</Text>
        </TouchableHighlight>
    </View>
</View>

<View style={styles.filtersdesc}>
    <View style={styles.filteroptionleft}>
        <TouchableHighlight style={styles.menutext}>
            <Text style={styles.menutextcolor}>PRICE</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.menutext}>
            <Text style={styles.menutextcolor}>COLOR</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.menutext}>
            <Text style={styles.menutextcolor}>SLEEVE LENGTH</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.menutext}>
            <Text style={styles.menutextcolor}>PROMOTIONS</Text>
        </TouchableHighlight>
    </View>
    <View style={styles.filteroptionright}>
        <View style={styles.filterlist}>
            <View style={styles.listinside}>
                <CheckBox value={true}/>
                <Text>AED 8 to AED 9</Text>
            </View>
            <View style={styles.listinside}>
                <CheckBox />
                <Text>AED 8 to AED 9</Text>
            </View>
            <View style={styles.listinside}>
                <CheckBox />
                <Text>AED 8 to AED 9</Text>
            </View>
            <View style={styles.listinside}>
                <CheckBox />
                <Text>AED 8 to AED 9</Text>
            </View>
        </View>
    </View>
</View>

</View>
}

{(this.state.sortby === true)&&
<View>
    <View style={styles.filtersmain}>
        <View style={styles.filterheaderleft}>
            <Text style={styles.sortbytext}>SORT BY</Text>
        </View>
        <View style={styles.filterheaderright}>
            <TouchableHighlight style={styles.closeclass} onPress={this.closelist}>
                <Text style={{color:'#000',fontWeight:'500'}}>X</Text>
            </TouchableHighlight>
        </View>
    </View>
    <View style={styles.sortbyborder}>
        <Picker style={styles.pickersort}>
            <Picker.Item label="Java" value="java" />
            <Picker.Item label="JavaScript" value="js" />
        </Picker>
    </View>
</View>
}


</View>
</ScrollView>
)
}
}

export default Plp;

const styles = StyleSheet.create({
    header:{
        color:'#000',
        fontSize:18,
        fontWeight:'500'
    },
    headerrt:{
        fontSize:16,
        color:'#767676'  
    },
    flowDirection:{
        flexDirection:'row',
        justifyContent: 'space-between',
        padding:10
    },
    plpmain:{
        paddingLeft:5,
        paddingRight:5,
        paddingBottom:10,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection:'row',
        justifyContent:'space-between',
        flex: 1
    },
    plpgrid:{
        width:'50%',
        position:'relative',
        padding:5
    },
    plpimage:{
        height:200,
        width:'100%'
    },
    newplpimage:{
        position:'absolute',
        bottom:0,
        left:0
    },
    plpgridtext:{
        color:'#000',
        fontSize:16
    },
    plpaed:{
        color:'#000',
        fontSize:18,
        paddingTop:5,
        fontWeight:'500'
    },
    likeplpimage:{
        position:'absolute',
        top:10,
        right:10
    },
    changeproduct:{
        paddingTop:5,
        backgroundColor:'#000',
        flexDirection:'row'
    },
    leftfilter:{
        width:'50%',
        padding:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    filterText:{
        color:'#fff',
        paddingLeft:10
    },
    filtersmain:{
        flexDirection:'row',
        paddingBottom:10
    },
    filterheaderleft:{
        paddingTop:0,
        paddingLeft:10,
        paddingRight:10,
        width:'50%',
        flexDirection:'row'
    },
    filterheaderright:{
        paddingLeft:10,
        paddingRight:10,
        paddingTop:0,
        paddingBottom:0,
        width:'50%'
    },
    filterheadertext:{
        paddingLeft:10,
        fontWeight:'500',
        fontSize:16,
        color:'#000'
    },
    filterheadertextx:{
        fontWeight:'500',
        fontSize:16,
        color:'#000'
    },
    applybutton:{
        backgroundColor:'#000',
        paddingTop:5,
        paddingBottom:5,
        width:100,
        left:40,
        top:0
    },
    filtersdesc:{
        borderWidth:0.5,
        borderTopColor:'#e4e4e4',
        borderLeftWidth:0,
        borderRightWidth:0,
        borderBottomWidth:0,
        width:'100%',
        flexDirection:'row',
        minHeight:400
    },
    filteroptionleft:{
        width:'50%',
        backgroundColor:'#f3f4f6'
    },
    menutext:{
        width:'100%',
        borderBottomColor:'#e3e6e2',
        borderBottomWidth:0.5
    },
    menutextcolor:{
        padding:10,
        color:'#000',
    },
    filterlist:{
        padding:10
    },
    filteroptionright:{
        width:'50%',
        backgroundColor:'#fff'
    },
    listinside:{
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeclass:{
        alignItems: 'flex-end'
    },
    sortbytext:{
        fontWeight:'500',
        fontSize:16,
        color:'#000'
    },
    sortbyborder:{
        borderWidth:0.5,
        borderTopColor:'#e4e4e4',
        width:'100%',
        padding:10,
        borderBottomColor:0,
        borderLeftWidth:0,
        borderRightWidth:0,
        minHeight:350
    },
    pickersort:{
        width:'100%',
        backgroundColor:'#f3f4f6'
    }
})